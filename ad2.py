f = open("input2.txt")
directions = f.read().split('\n')

numpad = [[1,2,3],[4,5,6],[7,8,9]]

current_hor = 1
current_ver = 1

code = ""

for direction in directions: 

	for letter in direction:
		if letter == 'U':
			current_ver = max(0, current_ver - 1)
		elif letter == 'D':
			current_ver = min(2, current_ver + 1)
		elif letter == 'L':
			current_hor = max(0, current_hor - 1)
		elif letter == 'R':
			current_hor = min(2, current_hor + 1)

		print "Letter " + letter + " leads us to " + str(numpad[current_ver][current_hor])

	code = code + str(numpad[current_ver][current_hor])

print "This is the final code: " + code