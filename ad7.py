__author__ = 'ageier'

'''
--- Day 7: Internet Protocol Version 7 ---
While snooping around the local network of EBHQ, you compile a list of IP addresses (they're IPv7, of course; IPv6 is much too limited). You'd like to figure out which IPs support TLS (transport-layer snooping).
An IP supports TLS if it has an Autonomous Bridge Bypass Annotation, or ABBA. An ABBA is any four-character sequence which consists of a pair of two different characters followed by the reverse of that pair, such as xyyx or abba. However, the IP also must not have an ABBA within any hypernet sequences, which are contained by square brackets.
How many IPs in your puzzle input support TLS?
'''

def get_input():
	f = open("input7.txt")
	return f.read().split('\n')

lines = get_input()
in_brackets = False
number_of_TLS_supporting_lines = 0

for line in lines:
	supports_TLS = False
	has_ABBA_in_brackets = False
	for index,letter in enumerate(line):
		if letter =="[":
			in_brackets = True
		elif letter == "]":	
			in_brackets = False
		elif index + 1 != len(line) and letter == line[index+1]:
			try:
				if index != 0 and line[index+2] == line[index -1] and letter != line[index+2]:
					if in_brackets:
						has_ABBA_in_brackets = True
					else: 
						supports_TLS = True
					# print "Found ABBA", line[index-1], letter, line[index+1], line[index+2], "at index ", index
			except(IndexError):
				pass

	if supports_TLS and not has_ABBA_in_brackets:
		number_of_TLS_supporting_lines = number_of_TLS_supporting_lines + 1
		print line, "supports TLS"
	else:
		pass

print "A total number of", number_of_TLS_supporting_lines, "support TLS."
