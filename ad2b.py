f = open("input2.txt")
directions = f.read().split('\n')

numpad = [[0,0,1,0,0],[0,2,3,4,0],[5,6,7,8,9],[0,'A','B','C',0],[0,0,'D',0,0]]

current_hor = 0
current_ver = 2

code = ""

for direction in directions: 

	for letter in direction:
		if letter == 'U':
			cur_current_ver = max(0, current_ver - 1)
			if (numpad[cur_current_ver][current_hor]) != 0:
				current_ver = cur_current_ver
		elif letter == 'D':
			cur_current_ver = min(4, current_ver + 1)
			if (numpad[cur_current_ver][current_hor]) != 0:
				current_ver = cur_current_ver
		elif letter == 'L':
			cur_current_hor = max(0, current_hor - 1)
			if (numpad[current_ver][cur_current_hor]) != 0:
				current_hor = cur_current_hor
		elif letter == 'R':
			cur_current_hor = min(4, current_hor + 1)
			if (numpad[current_ver][cur_current_hor]) != 0:
				current_hor = cur_current_hor

		print "Letter " + letter + " leads us to " + str(numpad[current_ver][current_hor])

	code = code + str(numpad[current_ver][current_hor])

print "This is the final code: " + code