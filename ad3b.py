__author__ = 'ageier'

'''
Now that you've helpfully marked up their design documents, it occurs to you that triangles are specified in groups of three vertically. Each set of three numbers in a column specifies a triangle. Rows are unrelated.

For example, given the following specification, numbers with the same hundreds digit would be part of the same triangle:

101 301 501
102 302 502
103 303 503
201 401 601
202 402 602
203 403 603
In your puzzle input, and instead reading by columns, how many of the listed triangles are possible?

Your puzzle answer was 1826.
'''

def is_triangle(dimensions):
	a = int(dimensions[0])
	b = int(dimensions[1])
	c = int(dimensions[2])

	return a+b > c and b+c > a and c+a > b

f = open("input3.txt")
triangles = f.read().split('\n')

number_of_triangles = 0;

for i in range(len(triangles)/3): 
	row_1 = triangles[0+i*3].split()
	row_2 = triangles[1+i*3].split()
	row_3 = triangles[2+i*3].split() 

	for index in range(3):
		if is_triangle([row_1[index], row_2[index], row_3[index]]):
			number_of_triangles = number_of_triangles + 1

print "This is the number of possible triangles: " + str(number_of_triangles)