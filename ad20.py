__author__ = 'ageier'

'''
--- Day 20: Firewall Rules ---

You'd like to set up a small hidden computer here so you can use it to get back into the network later. However, the corporate firewall only allows communication with certain external IP addresses.
You've retrieved the list of blocked IPs from the firewall, but the list seems to be messy and poorly maintained, and it's not clear which IPs are allowed. Also, rather than being written in dot-decimal notation, they are written as plain 32-bit integers, which can have any value from 0 through 4294967295, inclusive.
The blacklist specifies ranges of IPs (inclusive of both the start and end value) that are not allowed. Then, the only IPs that this firewall allows are 3 and 9, since those are the only numbers not in any range.
Given the list of blocked IPs you retrieved from the firewall (your puzzle input), what is the lowest-valued IP that is not blocked?
'''

def split_and_sort(input_array):
	output = []
	for line in input_array:
		if line != "":
			string_line = line.split("-")
			string_line[0] = int(string_line[0])
			string_line[1] = int(string_line[1])
			output.append(string_line)
	return sorted(output, key=lambda x: x[0])

def get_input():
	f = open("input20.txt")
	return split_and_sort(f.read().split('\n'))

blacklist =get_input()
current_max_min = blacklist[0][1]

for line in blacklist:
	if(line[0]-1<=current_max_min):
		current_max_min = max(line[1], current_max_min)
	else:
		print "Lowest not blocked IP-Address is", current_max_min +1
		break;