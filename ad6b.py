__author__ = 'ageier'

from collections import OrderedDict

'''
--- Day 6: Signals and Noise ---
Something is jamming your communications with Santa. Fortunately, your signal is only partially jammed, and protocol in situations like this is to switch to a simple repetition code to get the message through.
In this model, the same message is sent repeatedly. You've recorded the repeating message signal (your puzzle input), but the data seems quite corrupted - almost too badly to recover. Almost.
All you need to do is figure out which character is most frequent for each position.

--- Part Two ---
Of course, that would be the message - if you hadn't agreed to use a modified repetition code instead.
In this modified code, the sender instead transmits what looks like random data, but for each character, the character they actually want to send is slightly less likely than the others. Even after signal-jamming noise, you can look at the letter distributions in each column and choose the least common letter to reconstruct the original message.
In the above example, the least common character in the first column is a; in the second, d, and so on. Repeating this process for the remaining characters produces the original message, advent.
Given the recording in your puzzle input and this new decoding methodology, what is the original message that Santa is trying to send?
'''

def get_input():
	f = open("input6.txt")
	return f.read().split('\n')

def get_unique_letters(input):
	return "".join(sorted(OrderedDict.fromkeys(input)))

def get_most_frequent_letter(input):
	counter=[]
	for letter in get_unique_letters(input):
		counter.append([letter, input.count(letter)])
	sorted_list = sorted(counter, key=lambda x: -x[1])
	return sorted_list[-1][0]

lines = get_input()
characters_per_column=['']*8
password = ''

for line in lines: 
	for index,character in enumerate(line):
		characters_per_column[index] = characters_per_column[index] + character

for column in characters_per_column:
	password = password + get_most_frequent_letter(column)

print "The password is", password