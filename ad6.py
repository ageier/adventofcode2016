__author__ = 'ageier'

from collections import OrderedDict

'''
--- Day 6: Signals and Noise ---
Something is jamming your communications with Santa. Fortunately, your signal is only partially jammed, and protocol in situations like this is to switch to a simple repetition code to get the message through.
In this model, the same message is sent repeatedly. You've recorded the repeating message signal (your puzzle input), but the data seems quite corrupted - almost too badly to recover. Almost.
All you need to do is figure out which character is most frequent for each position.
'''

def get_input():
	f = open("input6.txt")
	return f.read().split('\n')

def get_unique_letters(input):
	return "".join(sorted(OrderedDict.fromkeys(input)))

def get_most_frequent_letter(input):
	counter=[]
	for letter in get_unique_letters(input):
		counter.append([letter, input.count(letter)])
	sorted_list = sorted(counter, key=lambda x: -x[1])
	return sorted_list[0][0]

lines = get_input()
characters_per_column=['']*8
password = ''

for line in lines: 
	for index,character in enumerate(line):
		characters_per_column[index] = characters_per_column[index] + character

for column in characters_per_column:
	password = password + get_most_frequent_letter(column)

print "The password is", password