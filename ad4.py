__author__ = 'ageier'

from collections import OrderedDict

'''
--- Day 4: Security Through Obscurity ---

Finally, you come across an information kiosk with a list of rooms. Of course, the list is encrypted and full of decoy data, but the instructions to decode the list are barely hidden nearby. Better remove the decoy data first.

Each room consists of an encrypted name (lowercase letters separated by dashes) followed by a dash, a sector ID, and a checksum in square brackets.

A room is real (not a de coy) if the checksum is the five most common letters in the encrypted name, in order, with ties broken by alphabetization. For example:

aaaaa-bbb-z-y-x-123[abxyz] is a real room because the most common letters are a (5), b (3), and then a tie between x, y, and z, which are listed alphabetically.
a-b-c-d-e-f-g-h-987[abcde] is a real room because although the letters are all tied (1 of each), the first five are listed alphabetically.
not-a-real-room-404[oarel] is a real room.
totally-real-room-200[decoy] is not.
Of the real rooms from the list above, the sum of their sector IDs is 1514.

What is the sum of the sector IDs of the real rooms?
'''

def get_rooms():
	f = open("input4.txt")
	return f.read().split('\n')	

class FancyRoom:

	def extract_room_number(self, input):
		self.room_number = input.split('[')[-2].split('-')[-1]

	def extract_checksum(self,input):
		self.checksum = (input.split('[')[-1]).split(']')[-2]

	def extract_room_name(self, input):
		self.room_name = input.split('[')[-2].split('-')
		self.room_name.remove(self.room_number)
		self.room_name = ''.join(self.room_name)
		print (self.room_name)

	def calculate_checksum(self):
		counter=[]
		for letter in self.get_unique_letters():
			counter.append([letter, self.room_name.count(letter)])
		sorted_list = sorted(counter, key=lambda x: -x[1])
		self.real_checksum = ''.join(i[0] for i in sorted_list[0:5])

	def get_unique_letters(self):
		return "".join(sorted(OrderedDict.fromkeys(self.room_name)))

	def control_checksum(self):
		print "Room ", self.room_number, "has checksums", self.checksum, self.real_checksum
		if(self.checksum == self.real_checksum):
			return int(self.room_number)
		else:
			return 0

	def __init__(self, input):
		self.extract_room_number(input)
		self.extract_checksum(input)
		self.extract_room_name(input)
		self.calculate_checksum()


rooms = get_rooms()
room_sum = 0;

for room in rooms:
	fancyRoom = FancyRoom(room)
	room_sum = room_sum + fancyRoom.control_checksum()

print "The total sum is ", str(room_sum)
