__author__ = 'ageier'

'''
--- Day 4: Security Through Obscurity ---
--- Part Two ---

With all the decoy data out of the way, it's time to decrypt this list and get moving.
The room names are encrypted by a state-of-the-art shift cipher, which is nearly unbreakable without the right software. However, the information kiosk designers at Easter Bunny HQ were not expecting to deal with a master cryptographer like yourself.
To decrypt a room name, rotate each letter forward through the alphabet a number of times equal to the room's sector ID. A becomes B, B becomes C, Z becomes A, and so on. Dashes become spaces.
For example, the real name for qzmt-zixmtkozy-ivhz-343 is very encrypted name.
What is the sector ID of the room where North Pole objects are stored?
'''

def get_rooms():
	f = open("input4.txt")
	return f.read().split('\n')	

class FancyRoom:

	def extract_room_number(self, input):
		self.room_number = input.split('[')[-2].split('-')[-1]

	def extract_checksum(self,input):
		self.checksum = (input.split('[')[-1]).split(']')[-2]

	def extract_room_name(self, input):
		self.room_name = input.split('[')[-2].split('-')
		self.room_name.remove(self.room_number)
		self.room_name = ' '.join(self.room_name)

	def get_real_room_name(self):
		real_name = ''
		shift = int(self.room_number) % 26

		for letter in self.room_name:
			if(letter != ' '):
				index = ALPHABET.index(letter) + shift
				if(index >= 26):
					index = index - 26
				real_name = real_name + ALPHABET[index]	
			else:
				real_name = real_name + ' '
		self.real_room_name = real_name

	def is_northpole_storage(self):
		try:
			indexx = self.real_room_name.index('northpole')
			return True
		except ValueError:
			return False

	def __init__(self, input):
		self.extract_room_number(input)
		self.extract_checksum(input)
		self.extract_room_name(input)
		self.get_real_room_name()

ALPHABET = 'abcdefghijklmnopqrstuvwxyz'
rooms = get_rooms()

for room in rooms:
	fancyRoom = FancyRoom(room)
	if fancyRoom.is_northpole_storage():
		print "The Room ", fancyRoom.real_room_name, " has room number ", str(fancyRoom.room_number)
