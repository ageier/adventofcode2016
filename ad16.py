__author__ = 'ageier'

'''
--- Day 16: Dragon Checksum ---
You're done scanning this part of the network, but you've left traces of your presence. You need to overwrite some disks with random-looking data to cover your tracks and update the local security system with a new checksum for those disks.
For the data to not be suspicious, it needs to have certain properties; purely random data will be detected as tampering. To generate appropriate random data, you'll need to use a modified dragon curve.
Start with an appropriate initial state (your puzzle input). Then, so long as you don't have enough data yet to fill the disk, repeat the following steps:
Call the data you have at this point "a".
Make a copy of "a"; call this copy "b".
Reverse the order of the characters in "b".
In "b", replace all instances of 0 with 1 and all 1s with 0.
The resulting data is "a", then a single 0, then "b".
'''

def get_reverse_fill(fill):
	output = ''
	for letter in fill[::-1]:
		output += str(int(not bool(int(letter))))
	return output

def calculate_checksum(filling):
	if len(filling) % 2 == 0:
		output = ''
		for i in range(len(filling)/2):
			output += str(int((bool(int(filling[i*2])) == bool(int(filling[i*2+1])))))
		calculate_checksum(output)
	else:
		print "The checksum is " + filling


input = '10111011111001111'
length = 272
fill = input

while len(fill) < length:
	fill = fill + '0' + get_reverse_fill(fill)

calculate_checksum(fill[0:length])



