def save_history():
	vertical.append(length_vertical)
	horizontal.append(length_horizontal)

def check_location():
	for i in range(len(horizontal)-1):
		if (horizontal[i] == length_horizontal and vertical[i] == length_vertical):
			print "You have been here before"
			print str(length_horizontal) +" "+ str(length_vertical)
		

f = open("input1.txt")

directions = f.read().split(', ')
total_nav = 'N'

length_horizontal = 0
length_vertical = 0

vertical = [0]
horizontal = [0]

for direction in directions:

	nav = direction[0]

	if len(direction) > 2:
		step = int(direction[1:])
	else: 
		step = int(direction[1])


	if total_nav == 'N':
		if nav == 'L':
			length_horizontal = length_horizontal - step
			total_nav = 'W'
		else: 
			length_horizontal = length_horizontal + step
			total_nav = 'E'

	elif total_nav == 'E':
		if nav == 'L':
			length_vertical = length_vertical + step
			total_nav = 'N'
		else:
			total_nav = 'S'
			length_vertical = length_vertical - step

	elif total_nav == 'S':
		if nav == 'L':
			length_horizontal = length_horizontal + step
			total_nav = 'E'
		else:
			length_horizontal = length_horizontal - step
			total_nav = 'W'

	elif total_nav == 'W':
		if nav == 'L':
			total_nav = 'S'
			length_vertical = length_vertical - step
		else:
			total_nav = 'N'
			length_vertical = length_vertical + step


	print str(length_horizontal) +" "+ str(length_vertical)		
	check_location()
	save_history()

print abs(length_horizontal) + abs(length_vertical)