__author__ = 'ageier'

'''
--- Day 12: Leonardo's Monorail ---
You finally reach the top floor of this building: a garden with a slanted glass ceiling. Looks like there are no more stars to be had.
While sitting on a nearby bench amidst some tiger lilies, you manage to decrypt some of the files you extracted from the servers downstairs.
According to these documents, Easter Bunny HQ isn't just this building - it's a collection of buildings in the nearby area. They're all connected by a local monorail, and there's another building not far from here! Unfortunately, being night, the monorail is currently not operating.
You remotely connect to the monorail control systems and discover that the boot sequence expects a password. The password-checking logic (your puzzle input) is easy to extract, but the code it uses is strange: it's assembunny code designed for the new computer you just assembled. You'll have to execute the code and get the password.
The assembunny code you've extracted operates on four registers (a, b, c, and d) that start at 0 and can hold any integer. However, it seems to make use of only a few instructions:
cpy x y copies x (either an integer or the value of a register) into register y.
inc x increases the value of register x by one.
dec x decreases the value of register x by one.
jnz x y jumps to an instruction y away (positive means forward; negative means backward), but only if x is not zero.
The jnz instruction moves relative to itself: an offset of -1 would continue at the previous instruction, while an offset of 2 would skip over the next instruction.
After executing the assembunny code in your puzzle input, what value is left in register a?
'''

global index
index = 0

def get_input():
	f = open("input12.txt")
	return f.read().split('\n')[0:-1]

def init_registers():
	register['a'] = 0
	register['b'] = 0
	register['c'] = 0 
	register['d'] = 0

def handle_instruction(instruction):
	splitted = instruction.split()
	if splitted[0] == 'cpy':
		handle_copy(splitted)
	elif splitted[0] == 'inc':
		inc_register(splitted[1])
	elif splitted[0] == 'dec':
		dec_register(splitted[1])
	elif splitted[0] == 'jnz':
		handle_loop(splitted)

def handle_copy(splitted_instruction):
	if register.get(splitted_instruction[1]) != None:
		copy_register(splitted_instruction[1], splitted_instruction[2])
	else:
		copy_number(splitted_instruction[1], splitted_instruction[2])

def handle_loop(splitted_instruction):
	global index
	try:
		if register[splitted_instruction[1]] != 0:
			index = index + int(splitted_instruction[2]) - 1
	except(KeyError):
		if int(splitted_instruction[1]) != 0:
			index = index + int(splitted_instruction[2]) - 1

def copy_number(number, registers):
	register[registers] = number

def copy_register(register_from, register_to):
	register[register_to] = register[register_from]

def inc_register(registers):
	register[registers] = int(register[registers]) + 1

def dec_register(registers):
	register[registers] = int(register[registers]) -1

register = {}
instructions = get_input()
init_registers()

while index < len(instructions):
	handle_instruction(instructions[index])
	index += 1

print register['a'] 

