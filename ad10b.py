__author__ = 'ageier'

'''
--- Day 10: Balance Bots ---
You come upon a factory in which many robots are zooming around handing small microchips to each other.
Upon closer examination, you notice that each bot only proceeds when it has two microchips, and once it does, it gives each one to a different bot or puts it in a marked "output" bin. Sometimes, bots take microchips from "input" bins, too.
Inspecting one of the microchips, it seems like they each contain a single number; the bots must use some logic to decide what to do with each chip. You access the local control computer and download the bots' instructions (your puzzle input).
What do you get if you multiply together the values of one chip in each of outputs 0, 1, and 2?
'''

def receive_input(instruction):
	splitted = instruction.split()
	return splitted[1], splitted[6], splitted[5] == "bot",  splitted[11], splitted[10] == "bot"

class Bot:
	def __init__(self, instruction):
		self.number, self.lower_bot, self.lower_is_bot, self.higher_bot, self.higher_is_bot = receive_input(instruction)
		self.values = []
		self.create_outputs()
		print "Created Bot " + str(receive_input(instruction))

	def add_value(self, value):
		self.values.append(value)

	def distribute_values(self):
		if len(self.values) == 2:
			self.values = sorted(self.values)

			if self.lower_is_bot:
				print "Bot " + self.number + " distributes " + str(self.values[0]) + " to Bot " + bots[self.lower_bot].number
				bots[self.lower_bot].add_value(self.values[0])
			else:
				outputs[self.lower_bot].append(self.values[0])

			if self.higher_is_bot:
				print "Bot " + self.number + " distributes " + str(self.values[1]) + " to Bot " +bots[self.higher_bot].number
				bots[self.higher_bot].add_value(self.values[1])
			else:
				outputs[self.higher_bot].append(self.values[1])
			
			self.values = []
			bots[self.lower_bot].distribute_values()
			bots[self.higher_bot].distribute_values()

	def create_outputs(self):
		if not self.lower_is_bot:
			if outputs.get(self.lower_bot) == None:
				outputs[self.lower_bot] = []
		if not self.higher_is_bot:
			if outputs.get(self.higher_bot) == None:
				outputs[self.higher_bot] = []

def get_input():
	f = open("input10.txt")
	return sorted(f.read().split('\n'))

def handle_instruction(instruction):
	if instruction != "":
		ins_type = instruction.split()[0]
		if(ins_type == "bot"):
			handle_bot(instruction)
		elif(ins_type == "value"):
			handle_value(instruction)

def handle_bot(instruction):
	bot = Bot(instruction)
	bots[bot.number] = bot

def handle_value(instruction):
	splitted = instruction.split()
	bots[splitted[5]].add_value(int(splitted[1]))
	print "Add to Bot " + splitted[5] + " value " + str(splitted[1])

def start_distribution():
	bots['208'].distribute_values()


bots = {}
outputs = {}
instructions = get_input()

for instruction in instructions:
	handle_instruction(instruction)

start_distribution()

print "The multiplied value is " + str((outputs['1'][0] * outputs['2'][0] * outputs['0'][0]))

