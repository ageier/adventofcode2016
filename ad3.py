def is_triangle(dimensions):
	a = int(dimensions[0])
	b = int(dimensions[1])
	c = int(dimensions[2])

	return a+b > c and b+c > a and c+a > b

f = open("input3.txt")
triangles = f.read().split('\n')

number_of_triangles = 0;

for triangle in triangles: 
	if is_triangle(triangle.split()):
		number_of_triangles = number_of_triangles + 1


print "This is the number of possible triangles: " + str(number_of_triangles)